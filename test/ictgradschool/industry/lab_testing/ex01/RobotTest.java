package ictgradschool.industry.lab_testing.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testTurnEast() {
        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());
    }
    @Test
    public void testTurnSouth(){
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());

    }
    @Test
    public void testTurnWest() {
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
    }

    @Test
    public void testTurnNorth(){
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());}

     @Test
    public void testMoveNorth(){
        try {
            myRobot.move();
            assertEquals(9,myRobot.row());
            assertEquals(1,myRobot.column() );
            assertEquals(Robot.Direction.North, myRobot.getDirection());
        }catch(IllegalMoveException e){fail();}

     }

     @Test
     public void testMoveEast(){
        try{
            myRobot.turn();
            myRobot.move();
            assertEquals(10,myRobot.currentState().row);
            assertEquals(2,myRobot.column());
            assertEquals(Robot.Direction.East, myRobot.getDirection());
        }catch (IllegalMoveException e){fail();}
     }

    @Test
    public void testIllegalMoveEast() {
        boolean atRight = false;
        myRobot.turn();
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
    }

    @Test
    public void testMoveWest(){
        try{
            myRobot.turn();
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(1,myRobot.currentState().column);
        }catch(IllegalMoveException e){fail();}
    }

    @Test
    public void testIllegalMoveWest(){
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        try{
            myRobot.move();
            fail();
        }catch(IllegalMoveException e){
            assertEquals(1,myRobot.currentState().column);
        }
    }

    @Test
    public void testMoveSouth(){
        try{
            myRobot.move();
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(9,myRobot.currentState().row);

        }catch(IllegalMoveException e){
            fail();
        }

    }

    @Test
    public void testIllegalMoveSouth(){
        myRobot.turn();
        myRobot.turn();
        try{myRobot.move();
        fail();
        }catch(IllegalMoveException e){
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testBacktrack(){
        try{
            myRobot.move();
            myRobot.turn();
            myRobot.move();
            myRobot.move();
            myRobot.backTrack();
            assertEquals(2,myRobot.currentState().column);
            assertEquals(9,myRobot.currentState().row);
            myRobot.backTrack();
            assertEquals(1, myRobot.currentState().column);
            assertEquals(9, myRobot.currentState().row);
            assertEquals(Robot.Direction.East, myRobot.currentState().direction);
            myRobot.backTrack();
            assertEquals(Robot.Direction.North,myRobot.currentState().direction);
            myRobot.backTrack();
            assertEquals(10, myRobot.currentState().row);
        }catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void testColumn(){
        assertEquals(1,myRobot.column());
    }
    @Test
    public void testRow(){
        assertEquals(10, myRobot.row());
    }
    @Test
    public void testCurrentState(){
        assertEquals(Robot.Direction.North, myRobot.currentState().direction);
        assertEquals(1,myRobot.currentState().column);
        assertEquals(10,myRobot.currentState().row);
    }
}
