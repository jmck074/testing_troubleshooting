package ictgradschool.industry.lab_testing.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {
private SimpleSpellChecker mySC;

    @Before
    public void setup(){
        try {

            mySC = new SimpleSpellChecker(new Dictionary(), "one two expedient fve one");

        }catch(InvalidDataFormatException e){
            e.getMessage();
            fail();

        }
    }


    @Test
    public void testFoundationsOfMathematics() {
        assertEquals(2, 1 + 1);
    }

    @Test
    public void testConstructor(){
        assertNotNull(mySC);
    }



    @Test
    public void testGetMisspelledWords(){
        assertEquals("fve",mySC.getMisspelledWords().get(0));
//test for correctly spelled words that may not be in dictionary
        assertFalse(mySC.getMisspelledWords().contains("expedient"));
}
@Test
    public void testGetUniqueWords(){
        assertEquals(4,mySC.getUniqueWords().size());
}

@Test
    public void testGetFrequencyOfWord(){
        try{
            assertEquals(1,mySC.getFrequencyOfWord("two"));
            assertEquals(2,mySC.getFrequencyOfWord("one"));
            assertEquals(0,mySC.getFrequencyOfWord("Jalapeno"));
        }catch(InvalidDataFormatException e){e.getMessage();}
}
}
/*@Test
    public void testGetFrequencyException(){
        try{

            assertEquals(0,mySC.getFrequencyOfWord(36));
            fail();
        }catch (InvalidDataFormatException e){assertTrue(true);}
    }
}*/

