package ictgradschool.industry.lab_testing.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * TODO Implement this class.
 */
public class TestDictionary {
    private Dictionary myDictionary;

    @Before
    public void setup(){myDictionary = new Dictionary();}


    @Test
    public void testTrueIsTrue() {
        assertEquals(true, true);
    }


    @Test
    public void testConstructor() {
        {
            assertNotNull(myDictionary);
            //Kien used String[] splitWords = WORDS.toLowerCase().split(",");
            //then assertEquals(splitWords.length, myDictionary.dictionary.size();

            //I think if I want to iterate through I can;t iterate through myDictionary since it is a dictionary object
            //BUT I can iterate through myDictionary.dictionary (also could set up an iterator)
        }


    }

    @Test
    public void testIsSpellingCorrectTrue(){
        assertEquals(true, myDictionary.isSpellingCorrect("hurricane"));
        assertEquals(true, myDictionary.isSpellingCorrect("similarity"));
    }

    @Test
    public void testIsSpellingCorrectFalse(){
        assertEquals(false, myDictionary.isSpellingCorrect("hurrikane"));
        assertEquals(false,myDictionary.isSpellingCorrect("simlarity"));
    }

    @Test
    public void testIsSpellingCorrectAbsent(){
        assertEquals(false,myDictionary.isSpellingCorrect("prop"));
    }
}