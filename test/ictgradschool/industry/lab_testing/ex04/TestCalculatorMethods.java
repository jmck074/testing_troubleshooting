package ictgradschool.industry.lab_testing.ex04;


import ictgradschool.industry.lab_testing.ex02.InvalidDataFormatException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

public class TestCalculatorMethods {
    private Calculator calc;

    @Before
    public void setUp() {calc = new Calculator();}

    @Test
    public void testConvertStringToDouble(){
        assertEquals(5.00, calc.convertStringToDouble("5"));
    }

    @Test
    public void testConvertStringToDoubleException(){
        try{
            calc.converStringToDouble("abc");
            fail();
        }catch (InvalidDataFormatException e){
            assertEquals("Invalid data format", e.getMessage());
        }
    }

    @Test
    public void testAreaOfRectangle(){
        assertEquals(20.0,calc.areaRectangle(4.0,5.0));
        assertNotEquals(20.0 calc.areaRectangle(3.0,2.0));
    }




}
