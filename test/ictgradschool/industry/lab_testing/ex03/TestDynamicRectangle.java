package ictgradschool.industry.lab_testing.ex03;

import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestDynamicRectangle {
    private MockPainter painter;

@Before
    public void setUp() {painter = new MockPainter(); }

    @Test
    public void TestLeftEdgeStartEmpty(){
    DynamicRectangleShape shape = new DynamicRectangleShape(0,50,-5,0);
   shape.paint(painter);
   assertFalse(painter.toString().contains("fill"));
    shape.move(100,100);
    shape.paint(painter);
    assertTrue(painter.toString().contains("fill"));



    }

 @Test
    public void TestLeftEdgeWhileFilled() {
DynamicRectangleShape shape = new DynamicRectangleShape(0,50,5,0);
int bounces=0;
int storedDirection=shape.fDeltaX;
while(storedDirection==shape.fDeltaX){
    shape.move(100,100);
}
bounces+=1;
storedDirection=shape.fDeltaX;
while(storedDirection==shape.fDeltaX){
    shape.move(100,100);
}
bounces+=1;
shape.paint(painter);
assertFalse(painter.toString().contains("fill"));
     System.out.println(bounces);


 }

 @Test
    public void TestTopEdgeStartEmpty(){
    DynamicRectangleShape shape = new DynamicRectangleShape(50,0,0,-5);
    shape.paint(painter);
    assertFalse(painter.toString().contains("fill"));
    shape.move(100,100);
    shape.paint(painter);
    assertTrue(painter.toString().contains("fill"));
 }

}
