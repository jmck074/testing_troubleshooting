package ictgradschool.industry.lab_testing.ex03;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestOval {
    private MockPainter painter;

    @Before
    public void setUp(){painter = new MockPainter();}

    @Test
    public void testDefaultConstructor(){
        OvalShape shape = new OvalShape();
        shape.paint(painter);
        assertEquals("(oval 0,0,25,35)",painter.toString());
    }

    @Test
    public void testConstructorWithSpeedValues(){
        OvalShape shape = new OvalShape(1,2,3,4);
        shape.paint(painter);
        assertEquals(3, shape.getDeltaX());
        assertEquals(4,shape.getDeltaY());
        assertEquals("(oval 1,2,25,35)",painter.toString());
    }

@Test
    public void testConstructorWithAllArgs(){
        OvalShape shape = new OvalShape(1,2,30,40,5,6);
        shape.paint(painter);
        assertEquals(30, shape.getDeltaX());
        assertEquals(40, shape.getDeltaY());
        assertEquals("(oval 1,2,5,6)",painter.toString());
}
}
